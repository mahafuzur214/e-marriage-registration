﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EmarriageFinal.Models;
using EMarriageFinal.Models;

namespace EmarriageFinal.Controllers
{
    public class MarriageReg1Controller : Controller
    {
        private EMarriageBDContext db = new EMarriageBDContext();

        // GET: /MarriageReg1/
        public ActionResult Index()
        {
            return View(db.MarriageReg2.ToList());
        }

        // GET: /MarriageReg1/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MarriageReg2 marriagereg2 = db.MarriageReg2.Find(id);
            if (marriagereg2 == null)
            {
                return HttpNotFound();
            }
            return View(marriagereg2);
        }

        // GET: /MarriageReg1/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: /MarriageReg1/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="Id,MaleName,MaleFatherName,MaleMotherName,MaleAge,MaleNID,MaleAddress,FemaleName,FemaleFatherName,FemaleMotherName,FemaleNID,FemaleAge,FemaleAddress,RegDate,WitnessName1Address,WitnessName2Address,AmountOfMohur,PaidAmount,Religion,RegFee")] MarriageReg2 marriagereg2)
        {
            if (ModelState.IsValid)
            {
                db.MarriageReg2.Add(marriagereg2);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(marriagereg2);
        }

        // GET: /MarriageReg1/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MarriageReg2 marriagereg2 = db.MarriageReg2.Find(id);
            if (marriagereg2 == null)
            {
                return HttpNotFound();
            }
            return View(marriagereg2);
        }

        // POST: /MarriageReg1/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="Id,MaleName,MaleFatherName,MaleMotherName,MaleAge,MaleNID,MaleAddress,FemaleName,FemaleFatherName,FemaleMotherName,FemaleNID,FemaleAge,FemaleAddress,RegDate,WitnessName1Address,WitnessName2Address,AmountOfMohur,PaidAmount,Religion,RegFee")] MarriageReg2 marriagereg2)
        {
            if (ModelState.IsValid)
            {
                db.Entry(marriagereg2).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(marriagereg2);
        }

        // GET: /MarriageReg1/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MarriageReg2 marriagereg2 = db.MarriageReg2.Find(id);
            if (marriagereg2 == null)
            {
                return HttpNotFound();
            }
            return View(marriagereg2);
        }

        // POST: /MarriageReg1/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            MarriageReg2 marriagereg2 = db.MarriageReg2.Find(id);
            db.MarriageReg2.Remove(marriagereg2);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
