﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EMarriageFinal.Models;

namespace EmarriageFinal.Controllers
{
    public class MarriageRegController : Controller
    {
        private EMarriageBDContext db = new EMarriageBDContext();

        // GET: /MarriageReg/
        public ActionResult Index()
        {
            return View(db.MarriageRegistrations.ToList());
        }

        // GET: /MarriageReg/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MarriageRegistration marriageregistration = db.MarriageRegistrations.Find(id);
            if (marriageregistration == null)
            {
                return HttpNotFound();
            }
            return View(marriageregistration);
        }

        // GET: /MarriageReg/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: /MarriageReg/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="Id,MaleName,MaleFatherName,MaleMotherName,MaleAge,MaleNID,MaleAddress,FemaleName,FemaleFatherName,FemaleMotherName,FemaleNID,FemaleAge,FemaleAddress,RegDate,WitnessName1Address,WitnessName2Address,AmountOfMohur,PaidAmount,Religion,FirstWifeNID,RegFee")] MarriageRegistration marriageregistration)
        {
            if (ModelState.IsValid)
            {
                db.MarriageRegistrations.Add(marriageregistration);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(marriageregistration);
        }

        // GET: /MarriageReg/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MarriageRegistration marriageregistration = db.MarriageRegistrations.Find(id);
            if (marriageregistration == null)
            {
                return HttpNotFound();
            }
            return View(marriageregistration);
        }

        // POST: /MarriageReg/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="Id,MaleName,MaleFatherName,MaleMotherName,MaleAge,MaleNID,MaleAddress,FemaleName,FemaleFatherName,FemaleMotherName,FemaleNID,FemaleAge,FemaleAddress,RegDate,WitnessName1Address,WitnessName2Address,AmountOfMohur,PaidAmount,Religion,FirstWifeNID,RegFee")] MarriageRegistration marriageregistration)
        {
            if (ModelState.IsValid)
            {
                db.Entry(marriageregistration).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(marriageregistration);
        }

        // GET: /MarriageReg/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MarriageRegistration marriageregistration = db.MarriageRegistrations.Find(id);
            if (marriageregistration == null)
            {
                return HttpNotFound();
            }
            return View(marriageregistration);
        }

        // POST: /MarriageReg/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            MarriageRegistration marriageregistration = db.MarriageRegistrations.Find(id);
            db.MarriageRegistrations.Remove(marriageregistration);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
