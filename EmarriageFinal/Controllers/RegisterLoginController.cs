﻿using EMarriageFinal.Manager;
using EMarriageFinal.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EmarriageFinal.Controllers
{
    public class RegisterLoginController : Controller
    {
        RegisterLoginManager ARegisterLoginManager = new RegisterLoginManager();

        public ActionResult RegisterLogin()
        {
            return View();
        }
        [HttpPost]
        public ActionResult RegisterLogin(RegisterRegistration aRegisterRegistration)
        {
            if (ARegisterLoginManager.CheckRegister(aRegisterRegistration).Equals(1))
            {
                Response.Redirect("http://localhost:51290/MarriageReg1/Index");
               
            }
            else
            {
                ViewBag.Message = "Please enter valid Email or Password";
            }
            return View();
        }
	}
}