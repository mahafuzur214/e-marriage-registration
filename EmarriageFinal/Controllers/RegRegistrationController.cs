﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EMarriageFinal.Models;

namespace EmarriageFinal.Controllers
{
    public class RegRegistrationController : Controller
    {
        private EMarriageBDContext db = new EMarriageBDContext();

        // GET: /RegRegistration/
        public ActionResult Index()
        {
            return View(db.RegisterRegistrations.ToList());
        }

        // GET: /RegRegistration/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RegisterRegistration registerregistration = db.RegisterRegistrations.Find(id);
            if (registerregistration == null)
            {
                return HttpNotFound();
            }
            return View(registerregistration);
        }

        // GET: /RegRegistration/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: /RegRegistration/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="Id,Name,Email,Password,ConfirmPassword,PhoneNumber,NID,Division,Dristrict,Thana,AreaAddress,DateOfBirth")] RegisterRegistration registerregistration)
        {
            if (ModelState.IsValid)
            {
                db.RegisterRegistrations.Add(registerregistration);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(registerregistration);
        }

        // GET: /RegRegistration/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RegisterRegistration registerregistration = db.RegisterRegistrations.Find(id);
            if (registerregistration == null)
            {
                return HttpNotFound();
            }
            return View(registerregistration);
        }

        // POST: /RegRegistration/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="Id,Name,Email,Password,ConfirmPassword,PhoneNumber,NID,Division,Dristrict,Thana,AreaAddress,DateOfBirth")] RegisterRegistration registerregistration)
        {
            if (ModelState.IsValid)
            {
                db.Entry(registerregistration).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(registerregistration);
        }

        // GET: /RegRegistration/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RegisterRegistration registerregistration = db.RegisterRegistrations.Find(id);
            if (registerregistration == null)
            {
                return HttpNotFound();
            }
            return View(registerregistration);
        }

        // POST: /RegRegistration/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            RegisterRegistration registerregistration = db.RegisterRegistrations.Find(id);
            db.RegisterRegistrations.Remove(registerregistration);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
