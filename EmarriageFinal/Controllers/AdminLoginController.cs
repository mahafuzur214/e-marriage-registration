﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EMarriageFinal.Manager;
using EMarriageFinal.Models;

namespace EmarriageFinal.Controllers
{
    public class AdminLoginController : Controller
    {
        AdminManager adminManager = new AdminManager();
        
      
        public ActionResult AdminLogin()
        {
            return View();
        }
        [HttpPost]
        public ActionResult AdminLogin(Admin admin)
        {
            if (adminManager.CheckAdmin(admin).Equals(1))
            {
                Response.Redirect("http://localhost:51290/regregistration");
                
            }
            else
            {
                ViewBag.Message = "Please enter valid Email or Password";
            }
            return View();
        }
	}
}