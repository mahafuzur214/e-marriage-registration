namespace EmarriageFinal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class version3 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.RegisterRegistrations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Email = c.String(nullable: false),
                        Password = c.String(nullable: false, maxLength: 10),
                        ConfirmPassword = c.String(nullable: false, maxLength: 10),
                        PhoneNumber = c.String(nullable: false),
                        NID = c.String(nullable: false, maxLength: 17),
                        Division = c.String(nullable: false),
                        Dristrict = c.String(nullable: false),
                        Thana = c.String(nullable: false),
                        AreaAddress = c.String(nullable: false),
                        DateOfBirth = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.RegisterRegistrations");
        }
    }
}
