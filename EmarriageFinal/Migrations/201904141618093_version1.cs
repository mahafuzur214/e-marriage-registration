namespace EmarriageFinal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class version1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MarriageRegistrations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        MaleName = c.String(nullable: false),
                        MaleFatherName = c.String(nullable: false),
                        MaleMotherName = c.String(nullable: false),
                        MaleAge = c.Int(nullable: false),
                        MaleNID = c.String(nullable: false, maxLength: 17),
                        MaleAddress = c.String(nullable: false),
                        FemaleName = c.String(nullable: false),
                        FemaleFatherName = c.String(nullable: false),
                        FemaleMotherName = c.String(nullable: false),
                        FemaleNID = c.String(nullable: false, maxLength: 17),
                        FemaleAge = c.Int(nullable: false),
                        FemaleAddress = c.String(nullable: false),
                        RegDate = c.String(nullable: false),
                        WitnessName1Address = c.String(nullable: false),
                        WitnessName2Address = c.String(nullable: false),
                        AmountOfMohur = c.Int(nullable: false),
                        PaidAmount = c.Int(nullable: false),
                        Religion = c.String(nullable: false),
                        FirstWifeNID = c.String(nullable: false, maxLength: 17),
                        RegFee = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.MarriageRegistrations");
        }
    }
}
