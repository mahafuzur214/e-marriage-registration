﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EMarriageFinal.Models;
using EMarriageFinal.Gateway;

namespace EMarriageFinal.Manager
{
    public class AdminManager
    {
        AdminGateway adminGateway=new AdminGateway();
        public int CheckAdmin(Admin admin)
        {
            return adminGateway.CheckAdmin(admin);
        }
    }
}