﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EMarriageFinal.Models;
using EMarriageFinal.Gateway;
using EmarriageFinal.Models;

namespace EMarriageFinal.Manager
{
    public class CheckMarriedManager
    {
        CheckMarriedGateway aMarriedGateway=new CheckMarriedGateway();

        public int IsMarried(Check aCheck)
        {
            if (aMarriedGateway.IsMarried(aCheck).Equals("Yes"))
            {
                return 2;
            }
            else
            {
                return 1;
            }
        }
    }
}