﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EMarriageFinal.Models;
using EMarriageFinal.Gateway;

namespace EMarriageFinal.Manager
{
    public class RegisterRegistrationManager
    {
        RegisterRegistrationGateway aGateway=new RegisterRegistrationGateway();


        public int SaveRegisterRegistration(RegisterRegistration aRegisterRegistration)
        {
            if (aGateway.IsRegister(aRegisterRegistration).Equals("Yes"))
            {
                return 2;
            }
            else
            {
                 return aGateway.SaveRegisterRegistration(aRegisterRegistration);
            }
           
        }
    }
}