﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EMarriageFinal.Models;
using EMarriageFinal.Gateway;

namespace EMarriageFinal.Manager
{
    public class RegisterLoginManager
    {
        RegisterGateway aGateway=new RegisterGateway();

        public int CheckRegister(RegisterRegistration aRegisterRegistration)
        {
            return aGateway.CheckRegister(aRegisterRegistration);
        }
    }
}