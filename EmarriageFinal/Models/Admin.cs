﻿using System.ComponentModel.DataAnnotations;

namespace EMarriageFinal.Models
{
    public class Admin
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Please enter your email")]
        [RegularExpression(@"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", ErrorMessage = "Please enter a valid email")]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Please enter Password")]
        [StringLength(10, MinimumLength = 6, ErrorMessage = "Password must be 6-10 characters")]
        [Display(Name = "Passwords")]
        public string Password { get; set; }

    }
}