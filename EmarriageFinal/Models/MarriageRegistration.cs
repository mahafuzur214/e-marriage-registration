﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMarriageFinal.Models
{
    public class MarriageRegistration
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "*")]
        [Display(Name = "বরের নাম ")]
        public string MaleName { get; set; }

        [Required(ErrorMessage = "*")]
        [Display(Name = "বরের পিতার নাম ")]
        public string MaleFatherName { get; set; }

        [Required(ErrorMessage = "*")]
        [Display(Name = "বরের মাতার নাম ")]
        public string MaleMotherName { get; set; }

        [Required(ErrorMessage = "*")]
        [Display(Name = "বর এর বয়স ")]
        public int MaleAge { get; set; }

        [Required(ErrorMessage = "*")]
        [StringLength(17, MinimumLength = 17, ErrorMessage = " 17 Digit")]
        [Display(Name = "বরের NID")]
        public string MaleNID { get; set; }

        [Required(ErrorMessage = "*")]
        [Display(Name = "বরের ঠিকানা")]
        public string MaleAddress { get; set; }

        [Required(ErrorMessage = "*")]
        [Display(Name = "কনের নাম ")]
        public string FemaleName { get; set; }

        [Required(ErrorMessage = "*")]
        [Display(Name = "কনের পিতার  নাম ")]
        public string FemaleFatherName { get; set; }

        [Required(ErrorMessage = "*")]
        [Display(Name = "কনের মাতার নাম ")]
        public string FemaleMotherName { get; set; }

        [Required(ErrorMessage = "*")]
        [StringLength(17, MinimumLength = 17, ErrorMessage = "17 Digit")]
        [Display(Name = "কনের NID")]
        public string FemaleNID { get; set; }

        [Required(ErrorMessage = "*")]
        [Display(Name = "কনের বয়স ")]
        public int FemaleAge { get; set; }

        [Required(ErrorMessage = "*")]
        [Display(Name = "কনের ঠিকানা")]
        public string FemaleAddress { get; set; }

        [Required(ErrorMessage = "*")]
        [Display(Name = "নিবন্ধন এর তারিখ")]
        public string RegDate { get; set; }

        [Required(ErrorMessage = "*")]
        [Display(Name = "১ম সাক্ষীর নাম ঠিকানা")]
        public string WitnessName1Address { get; set; }

        [Required(ErrorMessage = "*")]
        [Display(Name = "২য় সাক্ষীর নাম ঠিকানা")]
        public string WitnessName2Address { get; set; }

        [Required(ErrorMessage = "*")]
        [Display(Name = "দেনমোহরের পরিমাণ")]
        public int AmountOfMohur { get; set; }

        [Required(ErrorMessage = "*")]
        [Display(Name = "নগদ দেনমোহর")]
        public int PaidAmount { get; set; }

        [Required(ErrorMessage = "*")]
        [Display(Name = "ধর্ম")]
        public string Religion { get; set; }

        [Required(ErrorMessage = "*")]
        [StringLength(17, MinimumLength = 17, ErrorMessage = " 17 Digit")]
        [Display(Name = "১ম স্ত্রীর NID")]
        public string FirstWifeNID { get; set; }

         [Required(ErrorMessage = "*")]
        [Display(Name = "পরিশোধিত রেজিস্ট্রি ফি")]
        public int RegFee { get; set; }


    }
}