﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace EMarriageFinal.Models
{
    public class EMarriageBDContext : DbContext
    {
        // You can add custom code to this file. Changes will not be overwritten.
        // 
        // If you want Entity Framework to drop and regenerate your database
        // automatically whenever you change your model schema, please use data migrations.
        // For more information refer to the documentation:
        // http://msdn.microsoft.com/en-us/data/jj591621.aspx
    
        public EMarriageBDContext() : base("name=EMarriageBDContext")
        {
        }

        public System.Data.Entity.DbSet<MarriageRegistration> MarriageRegistrations { get; set; }

        public System.Data.Entity.DbSet<EmarriageFinal.Models.MarriageReg2> MarriageReg2 { get; set; }

        public System.Data.Entity.DbSet<RegisterRegistration> RegisterRegistrations { get; set; }

        public System.Data.Entity.DbSet<EMarriageFinal.Models.Admin> Admins { get; set; }
    
    }
}
