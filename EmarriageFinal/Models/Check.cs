﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EmarriageFinal.Models
{
    public class Check
    {

        [Required(ErrorMessage = "Please enter Male NID")]
        [StringLength(18, MinimumLength = 18, ErrorMessage = "NID must be 18 Digit")]
        [Display(Name = "Male NID ")]
        public string MaleNID { get; set; }

        [Required(ErrorMessage = "Please enter Male NID")]
        [StringLength(18, MinimumLength = 18, ErrorMessage = "NID must be 18 Digit")]
        [Display(Name = "Female NID")]
        public string FemaleNID { get; set; }
    }
}