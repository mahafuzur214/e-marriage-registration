﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EMarriageFinal.Models
{
    public class RegisterRegistration
    {
        public int Id { get; set; }

       [Required(ErrorMessage = "*")]
       [Display(Name = "কাজীর নাম")]
        public string Name { get; set; }

        [Required(ErrorMessage = "ইমেল")]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "কাজীর ইমেল")]
         public string Email { get; set; }

        [Required(ErrorMessage = "*")]
        [StringLength(10, MinimumLength = 6, ErrorMessage = "Password must be 6-10 characters")]
        [Display(Name = "পাসওয়ার্ড")]
        public string Password { get; set; }

        [Required(ErrorMessage = "*")]
        [StringLength(10, MinimumLength = 6, ErrorMessage = "6-10 ")]
        [Compare("Password")]
        [Display(Name = "পাসওয়ার্ড নিশ্চিত")]
        public string ConfirmPassword { get; set; }

        [DataType(DataType.PhoneNumber)]
        [Display(Name = "ফোন নাম্বার ")]
        [Required(ErrorMessage = "*")]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{4})[-. ]?([0-9]{4})$", ErrorMessage = "Entered phone format is not valid.")]
        public string PhoneNumber { get; set; }

        [Required(ErrorMessage = "*")]
        [StringLength(17, MinimumLength = 17, ErrorMessage = " 17 Digit")]
        [Display(Name = "কাজীর NID")]
        public string NID { get; set; }


        [Required(ErrorMessage = "*")]
        [Display(Name = " বিভাগ")]
        public string Division { get; set; }

        [Required(ErrorMessage = "*")]
        [Display(Name = "জেলা")]
        public string Dristrict { get; set; }

        [Required(ErrorMessage = "*")]
        [Display(Name = " থানা")]
        public string Thana { get; set; }

        [Required(ErrorMessage = "*")]
        [Display(Name = "দায়িত্ত রত এলাকা")]
        public string AreaAddress { get; set; }

        [Required(ErrorMessage = "*")]
        [Display(Name = "জন্ম তারিখ")]
        public string DateOfBirth { get; set; }
    }
}